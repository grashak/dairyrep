package connectivity;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface UserClient {
    @GET("admin/login")
    Call<User> getUser(
            @Header("Authorization") String authHeader
    );
}

