package utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.ssapplication.R;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class DialogManeger {

    /**
     * Class name used for logging
     */
    private transient final String TAG = "DialogManager";

    /**
     * Map for storing dialogs corresponding to ID's
     */
    private Map<Integer, DialogBuilder> dialogMap;

    /**
     * Static class level variable for singleton class
     */
    private transient static DialogManeger mDialogManager;

    /**
     * Prepare key value data structure to maintain
     * all the dialogs
     */
    private void init() {
        dialogMap = new LinkedHashMap<Integer, DialogBuilder>() {};
    }

    /**
     * Animation rotation value
     * and Duration
     */
    private static final int ROTATION_VALUE = 360;
    private static final int DURATION = 900;

    /**
     * Method to get dialogManager instance
     *
     * @return
     */
    public static DialogManeger getInstance() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManeger();
            mDialogManager.init();
        }
        return mDialogManager;
    }

    /**
     * Resets static instance of utils.DialogManager
     */
    public void uninitialize(){
        mDialogManager = null; //NOPMD
    }

    /**
     * Parametrized constructor to acces this class.
     *
     * @param context Context to access application resources.
     */
    public DialogBuilder createDialog(final Context context) {
        DialogBuilder dialogBuilder = new DialogBuilder(context);
        return dialogBuilder;
    }

    /**
     * This memthod adds created dialog to map
     * @param dialogBuilder
     * @return
     */
    public int build(DialogBuilder dialogBuilder){
        final int key = dialogBuilder.hashCode();
        dialogMap.put(key, dialogBuilder);
        return key;
    }

    /**
     * This memthod shows dialog corresponding to key
     * @param key
     * @return
     */
    public void show(int key){
        Log.e(TAG, "In show");
        if(dialogMap.containsKey(key)){
            DialogBuilder dialogBuilder = dialogMap.get(key);
            dialogBuilder.getDialog().show();
        } else {
            Log.w(TAG, "No matching key found in Dialog Map");
        }
    }


    public boolean isAnyDialogOpen(){

        return !dialogMap.isEmpty();
    }
    /**
     * This method dismisses dialog corresponding to key
     * @param key
     * @return
     */
    public void dismiss(int key){
        Log.d(TAG, "In dismiss");
        if(dialogMap.containsKey(key)){
            DialogBuilder dialogBuilder = dialogMap.get(key);
            if(dialogBuilder.getDialog().isShowing()){
                dialogBuilder.getDialog().dismiss();
                Log.d(TAG, "Removing dialog fro map");
                dialogMap.remove(key);
                dialogBuilder = null; //NOPMD
            }
        } else {
            Log.w(TAG, "No matching key found in Dialog Map");
        }
    }

    /**
     * This memthod dismisses all dialogs in map
     * @return
     */
    public void dismissAll(){
        Log.e(TAG, "In dismissAll");
        Set<Integer> keySet = new HashSet<>(dialogMap.keySet());
        for(Integer key: keySet){
            dismiss(key);
        }
    }


    /**Log.e
     * {@link DialogBuilder} class to create alert dialog
     * */
    public static class DialogBuilder {
        /**
         * Context to access application resources.
         */
        private transient Context mContext;
        /**
         * Dialog instance.
         */
        private transient AlertDialog mDialog;
        /**
         * LinearLayout instance.
         */
        private transient LinearLayout mLayoutAddMilk;
        /**
         * TextView instance.
         */
        private transient TextView mTxtTitle;
        /**
         * EditText instance.
         */
        private transient EditText mEdtTxtAddMilk;
        /**
         * Button instance.
         */
        private transient Button mButtonAddMilk;

        /**
         * DialogBuilder Contructor
         */
        public DialogBuilder(Context mContext) {
            Log.e("DialogBuilder","Dialog is created successfully...");
            this.mContext = mContext;
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            final LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            final View dialogView = layoutInflater.inflate(R.layout.dialog_custom, null);
            builder.setView(dialogView);
            mLayoutAddMilk = (LinearLayout) dialogView.findViewById(R.id.ll_add_milk);
            mTxtTitle = (TextView) dialogView.findViewById(R.id.txt_title);
            mEdtTxtAddMilk = (EditText) dialogView.findViewById(R.id.edit_txt_add_milk);
            mButtonAddMilk = (Button) dialogView.findViewById(R.id.button_add_milk);
            mDialog = builder.create();
            //mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mDialog.setCancelable(false);
        }

        /**
         * This method shows add milk dialog.
         */
        public void addMilkDialog(final String title, View.OnClickListener onClickListener) {
            mTxtTitle.setText(title);
            mButtonAddMilk.setOnClickListener(onClickListener);
        }

        /**
         * This method gives prepared custome dialog..
         */
        public Dialog getDialog() {
            return mDialog;
        }
    }

}
