package com.example.android.ssapplication.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.ssapplication.R;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import connectivity.User;
import connectivity.UserClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button btLogin, btCancel;
    private EditText tvName, tvPassword;
    private TextView tvCount;

    int counter = 3;
    private UserLoginTask mAuthTask = null;
    public static String mToken = null;

    Retrofit.Builder builder = new Retrofit.Builder().baseUrl("http://139.59.89.57/api/")
            .addConverterFactory(GsonConverterFactory.create());

    Retrofit retrofit = builder.build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvName = (EditText) findViewById(R.id.tvName);
        tvPassword = (EditText) findViewById(R.id.tvPassword);
        btLogin = (Button) findViewById(R.id.btLogin);
        btCancel = (Button) findViewById(R.id.btCancel);
        tvCount = (TextView) findViewById(R.id.tvCount);
        tvCount.setVisibility(View.GONE);

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UserLoginTask().execute();
            }
        });

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String email = tvName.getText().toString();
        private final String password = tvPassword.getText().toString();

        @Override
        protected Boolean doInBackground(Void... voids) {

            UserClient userClient = retrofit.create(UserClient.class);
            String base = email + ":" + password;

            String authHeader = "Basic " + Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);
            Log.d("nikhil", "doInBackground: "+ authHeader);
            Call<User> call = userClient.getUser(authHeader);
            Log.d("nikhil", "doInBackground: call" + call);

            /*call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {

                        Log.d("nikhil", "doInBackground: response"+response.body().getmToken());
                        return;
                    }else {

                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                }
            });
*/            try {
                Response<User> response = call.execute();
                Log.d("nikhil", "doInBackground: response"+response.body().gettoken());
                if (response.isSuccessful()) {
                    mToken = response.body().gettoken();
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            mAuthTask = null;
            if (aBoolean) {
                Toast.makeText(MainActivity.this,"successful login", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(MainActivity.this,"incorrect credentials", Toast.LENGTH_SHORT).show();
                tvPassword.requestFocus();
            }

        }
    }
}


