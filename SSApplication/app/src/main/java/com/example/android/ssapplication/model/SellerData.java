package com.example.android.ssapplication.model;

import java.util.List;

public class SellerData {
    private List<SellerPOJO> sellerPOJOList;

    public SellerData(List<SellerPOJO> sellerPOJOList) {
        this.sellerPOJOList = sellerPOJOList;
    }

    public List<SellerPOJO> getSellerPOJOList() {
        return sellerPOJOList;
    }

    public void setSellerPOJOList(List<SellerPOJO> sellerPOJOList) {
        this.sellerPOJOList = sellerPOJOList;
    }
}
