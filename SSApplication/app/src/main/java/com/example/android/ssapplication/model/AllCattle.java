package com.example.android.ssapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllCattle {

        @SerializedName("cattle_data")
        @Expose
        private List<CattleDatum> cattleData = null;

        public List<CattleDatum> getCattleData() {
            return cattleData;
        }

        public void setCattleData(List<CattleDatum> cattleData) {
            this.cattleData = cattleData;
        }

    }


