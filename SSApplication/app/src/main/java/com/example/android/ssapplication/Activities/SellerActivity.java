package com.example.android.ssapplication.Activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import com.example.android.ssapplication.Api.GetAllSellers;
import com.example.android.ssapplication.R;
import com.example.android.ssapplication.model.SellerData;
import com.example.android.ssapplication.model.SellerPOJO;

import java.io.IOException;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import connectivity.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SellerActivity extends AppCompatActivity {
    RecyclerView allSellersView;

    Retrofit.Builder builder = new Retrofit.Builder().baseUrl("http://139.59.89.57/api/")
            .addConverterFactory(GsonConverterFactory.create());

    Retrofit retrofit = builder.build();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("nikhilSeller", "onCreate: ");
        setContentView(R.layout.all_sellers_layout);
        allSellersView = (RecyclerView) findViewById(R.id.all_sellers_list);
        new GetAllSellerTask().execute();
    }


    public class GetAllSellerTask extends AsyncTask<Void, Void, SellerPOJO> {

        @Override
        protected SellerPOJO doInBackground(Void... voids) {
            GetAllSellers getAllSellers = retrofit.create(GetAllSellers.class);
            String token = MainActivity.mToken;
            Call<SellerData> call = getAllSellers.getAllSellers(token);

            /*call.enqueue(new Callback<SellerPOJO>() {
                @Override
                public void onResponse(Call<SellerPOJO> call, Response<SellerPOJO> response) {
                    if (response.isSuccessful()) {
                        Log.d("nikhilSeller", "doInBackground: response"+response.body());
                    }
                }

                @Override
                public void onFailure(Call<SellerPOJO> call, Throwable t) {

                }
            });*/
            try {
                Response<SellerData> response = call.execute();
                if (response.isSuccessful()) {
                    Log.d("nikhilSeller", "doInBackground: response"+response.body().getSellerPOJOList());

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
