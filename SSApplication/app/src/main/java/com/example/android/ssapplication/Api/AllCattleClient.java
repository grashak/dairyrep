package com.example.android.ssapplication.Api;

import com.example.android.ssapplication.model.AllCattle;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AllCattleClient {
    @GET("cattle/milking")
    Call<AllCattle> getAllLactatingCattle();
}
