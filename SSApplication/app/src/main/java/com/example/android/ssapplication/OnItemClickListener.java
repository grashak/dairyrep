package com.example.android.ssapplication;

public interface OnItemClickListener {

    void onItemClick(int position);
}
