package com.example.android.ssapplication.Api;

import com.example.android.ssapplication.model.SellerData;
import com.example.android.ssapplication.model.SellerPOJO;

import java.util.ArrayList;

import connectivity.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface GetAllSellers {
    /*@GET("admin/login")
    Call<User> getUser(
            @Header("Authorization") String authHeader
    );*/
    @GET("seller/active/1")
    Call<SellerData> getAllSellers(@Header("x-access-token") String token);
}
