package com.example.android.ssapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.ssapplication.OnItemClickListener;
import com.example.android.ssapplication.R;
import com.example.android.ssapplication.model.CattleDatum;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class MilkAdapter extends RecyclerView.Adapter<MilkAdapter.ViewHolder> {

    private Context context;
    private List<CattleDatum> values;
    private OnItemClickListener mOnItemClickListener;

    public MilkAdapter(Context context, List<CattleDatum> values) {
        this.context = context;
        this.values = values;

    }

    public void setCattleList(List<CattleDatum> values) {
        this.values = values;
        notifyDataSetChanged();
    }

    @Override
    public MilkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_cattle, parent, false);
        MilkAdapter.ViewHolder viewHolder = new MilkAdapter.ViewHolder(view);
        context = parent.getContext();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MilkAdapter.ViewHolder holder, final int position) {
        CattleDatum cattleDatum = values.get(position);
        String message = String.valueOf(cattleDatum.getId());
        holder.textTvShow.setText(message);
        /*holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "The position is:" + position, Toast.LENGTH_SHORT).show();
                mOnItemClickListener.onItemClick(position);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textTvShow;
        CardView cv;

        public ViewHolder(View itemView) {
            super(itemView);
            textTvShow = (TextView) itemView.findViewById(R.id.list_item_cattle);
            /*cv = (CardView) itemView.findViewById(R.id.cv);*/
        }

    }
}
