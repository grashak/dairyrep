package com.example.android.ssapplication.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.android.ssapplication.Adapter.MilkAdapter;
import com.example.android.ssapplication.Api.AllCattleClient;
import com.example.android.ssapplication.OnItemClickListener;
import com.example.android.ssapplication.R;
import com.example.android.ssapplication.model.AllCattle;
import com.example.android.ssapplication.model.CattleDatum;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.DialogManeger;

public class MilkActivity extends AppCompatActivity implements OnItemClickListener {
    RecyclerView recyclerView;
    MilkAdapter milkAdapter;
    RecyclerView.LayoutManager layoutManager ;
    List<AllCattle> allCattle;
    List<CattleDatum> cattleDatum;

    /**
     * Key to represent progress dialog in Dialog manager
     */
    private transient int mDialogKey;
    /**
     * Variable for Dialog manager
     */
    private transient DialogManeger mDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_milk);
        recyclerView = (RecyclerView) findViewById(R.id.CattleRecycler);
        mDialogManager = DialogManeger.getInstance();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://139.59.89.57/api/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        milkAdapter = new MilkAdapter(MilkActivity.this,cattleDatum);

        AllCattleClient client = retrofit.create(AllCattleClient.class);
        Call<AllCattle> call = client.getAllLactatingCattle();

        call.enqueue(new Callback<AllCattle>() {
            @Override
            public void onResponse(Call<AllCattle> call, Response<AllCattle> response) {
                AllCattle repos = response.body();
                recyclerView.setAdapter(new MilkAdapter(MilkActivity.this, repos.getCattleData()));

            }

            @Override
            public void onFailure(Call<AllCattle> call, Throwable t) {
                Toast.makeText(MilkActivity.this, "error :(", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onItemClick(int position) {
       showAddMilkDialog();
    }

    private void showAddMilkDialog() {
        DialogManeger.DialogBuilder dialogBuilder = mDialogManager.createDialog(this);
        mDialogKey = mDialogManager.build(dialogBuilder);
        dialogBuilder.addMilkDialog("Add Milk", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogManager.dismiss(mDialogKey);
            }
        });
        mDialogManager.show(mDialogKey);
    }
}
